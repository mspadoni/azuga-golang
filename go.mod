module wrench.com/azuga

go 1.13

require (
	github.com/dgraph-io/ristretto v0.0.3
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.8
)
