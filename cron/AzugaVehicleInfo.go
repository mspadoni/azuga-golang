package main

import (
	"gorm.io/gorm/clause"
	"wrench.com/azuga/api"
	"wrench.com/azuga/client"
	"wrench.com/azuga/db"
	"wrench.com/azuga/db/models"
)

const (
	RESOURCE_KEY = "cron.AzugaVehicleInfoCollector"
	CONFIG_KEY   = "com.wrench.cron.azugavehicleinfocollector.interval"
)

func main() {
	db.InitDB()

	var azugaUsers = models.GetAzugaUsers()

	// seconds := config.GetLong(CONFIG_KEY, (15 * 60))
	// if util.LockApp(RESOURCE_KEY, seconds) {
	userCh := make(chan api.UserResponse)
	pageCh := make(chan api.UserResponse)

	firstPages := client.RequestAllUsersVehicles(userCh, pageCh, azugaUsers)
	allUserVehiclesMap := client.CollectPaginatedVehicles(pageCh, azugaUsers, firstPages)

	allVehicles := []models.AzugaVehicle{}
	for _, userVehicles := range allUserVehiclesMap {
		allVehicles = append(allVehicles, userVehicles...)
	}

	// Batch insert
	db.DB.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(&allVehicles)

	// Not sure if this can be improved with gorm
	// for userID, vehicles := range allUserVehiclesMap {
	// 	for _, vehicle := range vehicles {
	// 		db.DB.Model(models.Vehicle{}).Where("VIN = ?", vehicle.Vin).Where("UserId = ?", userID).Updates(models.Vehicle{Mileage: int(vehicle.Mileage)})
	// 	}
	// }
	// }
}
