package config

import (
	"strconv"

	"github.com/dgraph-io/ristretto"
	"wrench.com/azuga/db"
	"wrench.com/azuga/db/models"
)

var cache, _ = ristretto.NewCache(&ristretto.Config{
	NumCounters: 10000, // number of keys to track frequency of
	MaxCost:     1000,  // max entries
	BufferItems: 64,    // number of keys per Get buffer
})

func loadCache(key string, defaultValue string) string {
	value, rowsAffected := lookupKey(key)
	if rowsAffected == 0 { // using rowsAffected allows "" as a valid config Value
		value = defaultValue
	} else {
		cache.Set(key, value, 1)
	}
	return value
}

func lookupKey(key string) (string, int64) {
	var value string
	result := db.DB.Model(&models.Configuration{}).Select("Value").Where("ConfigKey = ?", key).Find(&value)
	if result.Error != nil {
		panic(result)
	}
	return value, result.RowsAffected
}

func Get(key string, defaultValue string) string {
	var strVal string
	cacheVal, ok := cache.Get(key)
	if ok {
		strVal = cacheVal.(string)
	} else {
		strVal = loadCache(key, defaultValue)
	}
	return strVal
}

func GetLong(key string, defaultValue int64) int64 {
	var strVal string
	cacheVal, ok := cache.Get(key)
	if ok {
		strVal = cacheVal.(string)
	} else {
		strVal = loadCache(key, strconv.FormatInt(defaultValue, 10))
	}
	value, err := strconv.ParseInt(strVal, 10, 64)
	if err != nil {
		panic(err)
	}
	return value
}

func GetBoolean(key string, defaultValue bool) bool {
	var strVal string
	cacheVal, ok := cache.Get(key)
	if ok {
		strVal = cacheVal.(string)
	} else {
		strVal = loadCache(key, strconv.FormatBool(defaultValue))
	}
	value, err := strconv.ParseBool(strVal)
	if err != nil {
		panic(err)
	}
	return value
}
