package client

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"wrench.com/azuga/api"
	"wrench.com/azuga/db/models"
)

// const AZUGA_URL_CONFIG_KEY = "com.wrench.3rdparty.azuga.url";
const azugaURL = "https://api.azuga.com/azuga-ws/v1/"
const azugaVehicleAPIPath string = "vehicle/view.json"

// semaphore to control max simultaneous goroutines hitting the http client
var maxRoutines = make(chan int, 5)

// RequestAllUsersVehicles - request all vehicles for all users with goroutines
func RequestAllUsersVehicles(userCh chan api.UserResponse,
	pageCh chan api.UserResponse,
	azugaUsers []models.AzugaUser) []api.UserResponse {

	for _, azugaUser := range azugaUsers {
		maxRoutines <- 1
		go func(user models.AzugaUser) {
			userCh <- getByUser(user, 1)
			<-maxRoutines
		}(azugaUser)
	}

	firstPages := []api.UserResponse{}
	for range azugaUsers {
		// userCh communicates "first page" responses (1 per user)
		// get one from the channel
		first := <-userCh
		firstPages = append(firstPages, first)
		requestAdditionalPages(pageCh, first)
	}
	close(userCh)
	return firstPages
}

// RequestAdditionalPages - request more pages with goroutines
func requestAdditionalPages(pageCh chan api.UserResponse, first api.UserResponse) {
	for page := 2; page <= first.Response.VehicleResponseVO.TotalPages; page++ {
		maxRoutines <- 1
		go func(pg int) {
			pageCh <- getByUser(first.User, pg)
			<-maxRoutines
		}(page)
	}
}

// CollectPaginatedVehicles - collect paginated data from channel
func CollectPaginatedVehicles(pageCh chan api.UserResponse,
	azugaUsers []models.AzugaUser,
	firstPages []api.UserResponse) map[uint][]models.AzugaVehicle {

	allUserVehiclesMap := make(map[uint][]models.AzugaVehicle)

	for _, first := range firstPages {
		allUserVehiclesMap[first.User.UserId] = first.Response.VehicleResponseVO.Vehicles
		vehicles := []models.AzugaVehicle{}
		for page := 2; page <= first.Response.VehicleResponseVO.TotalPages; page++ {
			addlResponse := <-pageCh // response from an additional page
			vehicles = append(vehicles, addlResponse.Response.VehicleResponseVO.Vehicles...)
		}
		allUserVehiclesMap[first.User.UserId] = append(allUserVehiclesMap[first.User.UserId], vehicles...)
	}
	close(pageCh)
	return allUserVehiclesMap
}

// getByUser - request vehicles for a user
func getByUser(azugaUser models.AzugaUser, page int) api.UserResponse {
	headers := make(map[string]string)
	queryParams := make(map[string]string)
	headers["Authorization"] = azugaUser.ApiKey
	queryParams["page"] = strconv.Itoa(page)

	userResponse := api.UserResponse{}
	userResponse.User = azugaUser
	userResponse.Response = get(azugaVehicleAPIPath, headers, queryParams)
	return userResponse
}

// Get - outbound http client request
func get(path string, headers map[string]string, queryParams map[string]string) api.AzugaVehicleResponse {
	request, err := http.NewRequest("GET", azugaURL+path, nil)
	if err != nil {
		panic(err)
	}

	for key, value := range headers {
		request.Header.Set(key, value)
	}

	query := request.URL.Query()
	for key, value := range queryParams {
		query.Add(key, value)
	}
	request.URL.RawQuery = query.Encode()

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	jsonResponse := ""
	if response.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			panic(err)
		}
		jsonResponse = string(bodyBytes)
	}

	var azugaVehicleResponse api.AzugaVehicleResponse
	err = json.Unmarshal([]byte(jsonResponse), &azugaVehicleResponse)
	if err != nil {
		panic(err)
	}
	return azugaVehicleResponse
}
