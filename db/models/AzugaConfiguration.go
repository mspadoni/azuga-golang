package models

// TableName - explicit
func (AzugaConfiguration) TableName() string {
	return "AzugaConfiguration"
}

// AzugaConfiguration -
type AzugaConfiguration struct {
	Id       uint   `gorm:"column:Id;size:11;primaryKey;autoIncrement"`
	UserId   uint   `gorm:"column:UserId;size:11;not null"`
	ApiKey   string `gorm:"column:ApiKey;type:varchar(50);not null"`
	IsActive bool   `gorm:"column:IsActive;default:true`
}
