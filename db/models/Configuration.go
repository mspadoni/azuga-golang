package models

import (
	"time"
)

// TableName - explicit
func (Configuration) TableName() string {
	return "Configuration"
}

type Configuration struct {
	Id         uint      `gorm:"column:Id;size:11;primaryKey;autoIncrement"`
	ConfigKey  string    `gorm:"column:ConfigKey;type:varchar(200);unique"`
	Value      string    `gorm:"column:Value;type:varchar(500)"`
	Notes      string    `gorm:"column:Notes;type:varchar(500)"`
	UpdateDate time.Time `gorm:"column:UpdateDate;type:datetime"`
	CreateDate time.Time `gorm:"column:CreateDate;type:datetime"`
}
