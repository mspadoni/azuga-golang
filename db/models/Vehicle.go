package models

import "time"

// TableName - explicit
func (Vehicle) TableName() string {
	return "Vehicle"
}

type Vehicle struct {
	Id               uint      `gorm:"column:Id;size:11;primaryKey;autoIncrement"`
	UserId           uint      `gorm:"column:UserId;size:11"`
	ProspectId       uint      `gorm:"column:ProspectId;size:11"`
	OrganizationId   uint      `gorm:"column:OrganizationId;size:11"`
	RegionId         uint      `gorm:"column:RegionId;size:11"`
	HubId            uint      `gorm:"column:HubId;size:11"`
	LocationId       uint      `gorm:"column:LocationId;size:11"`
	Label            string    `gorm:"column:Label;type:varchar(50)"`
	BaseId           int       `gorm:"column:BaseId;size:11"`
	Make             string    `gorm:"column:Make;type:varchar(30)"`
	MakeId           int       `gorm:"column:MakeId;size:11"`
	EpicorMakeId     int       `gorm:"column:EpicorMakeId;size:11"`
	Model            string    `gorm:"column:Model;type:varchar(50)"`
	ModelId          int       `gorm:"column:ModelId;size:11"`
	EpicorModelId    int       `gorm:"column:EpicorModelId;size:11"`
	Year             int16     `gorm:"column:Year;type:smallint;size:6"`
	VIN              string    `gorm:"column:VIN;type:varchar(20)"`
	Trim             string    `gorm:"column:Trim;type:varchar(100)"`
	TrimId           int       `gorm:"column:TrimId;size:11;"`
	Engine           string    `gorm:"column:Engine;type:varchar(100)"`
	EngineId         int       `gorm:"column:EngineId;size:11"`
	EpicorEngineId   int       `gorm:"column:EpicorEngineId;size:11"`
	Transmission     int8      `gorm:"column:Transmission;size:4`
	Drivetrain       int8      `gorm:"column:Drivetrain;size:4`
	License          string    `gorm:"column:License;type:varchar(10)"`
	State            string    `gorm:"column:State;type:varchar(10)"`
	MilesPerDay      int16     `gorm:"column:MilesPerDay;size:6`
	Mileage          int       `gorm:"column:Mileage;size:11;"`
	MileageDate      time.Time `gorm:"column:MileageDate;type:datetime`
	Type             int16     `gorm:"column:Type;size:6`
	Color            string    `gorm:"column:Color;type:varchar(50)`
	OilCapacity      float32   `gorm:"column:OilCapacity;type:decimal(4,2)"`
	FuelType         int8      `gorm:"column:FuelType;size:4`
	Damage           string    `gorm:"column:Damage;type:varchar(5000)`
	ExtraInfo        string    `gorm:"column:ExtraInfo;type:text`
	ExtraInfoVersion int8      `gorm:"column:ExtraInfoVersion;size:4`
	TireSizeId       uint      `gorm:"column:TireSizeId;size:11"`
	Region           int8      `gorm:"column:Region;size:4`
	Inactive         int8      `gorm:"column:Inactive;size:4`
	UpdateDate       time.Time `gorm:"column:UpdateDate;type:datetime`
	CreateDate       time.Time `gorm:"column:CreateDate;type:datetime`
}
