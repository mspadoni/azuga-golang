package models

import (
	"time"
)

// TableName - explicit
func (AppResource) TableName() string {
	return "AppResource"
}

type AppResource struct {
	Id         uint      `gorm:"column:Id;size:11;primaryKey;autoIncrement"`
	AppKey     string    `gorm:"column:ConfigKey;type:varchar(40)"`
	LockDate   time.Time `gorm:"column:LockDate;type:datetime"`
	CreateDate time.Time `gorm:"column:CreateDate;type:datetime"`
}
