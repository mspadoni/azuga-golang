package models

// TableName - explicit
func (AzugaVehicle) TableName() string {
	return "AzugaVehicle"
}

// AzugaVehicle -
type AzugaVehicle struct {
	Id           uint    `gorm:"column:Id;size:11;primaryKey;autoIncrement"`
	VehicleId    string  `gorm:"column:VehicleId;type:varchar(50);not null;unique" json:"vehicleId"`
	VehicleName  string  `gorm:"column:VehicleName;type:varchar(50)" json:"vehicleName"`
	VehicleType  string  `gorm:"column:VehicleType;type:varchar(20)" json:"vehicleType"`
	GroupName    string  `gorm:"column:GroupName;type:varchar(50)" json:"groupName"`
	Vin          string  `gorm:"column:Vin;type:varchar(20)" json:"vin"`
	Year         int16   `gorm:"column:Year;type:smallint" json:"year"`
	Make         string  `gorm:"column:Make;type:varchar(30)" json:"make"`
	Model        string  `gorm:"column:Model;type:varchar(30)" json:"model"`
	Mileage      float32 `gorm:"column:Mileage;type:decimal(10,4)" json:"odometer"`
	LicensePlate string  `gorm:"column:LicensePlate;type:varchar(10)" json:"licensePlate"`
	CostPerMile  float32 `gorm:"column:CostPerMile;type:decimal(10,4)" json:"costPerMile"`

	// These aren't part of the response so they break unmarshalling into this object
	// Can be ignored as long as the columns have DEFAULT CURRENT_TIMESTAMP
	// UpdateDate time.Time
	// CreateDate time.Time
}
