package models

import (
	"wrench.com/azuga/db"
)

// Derived Model
type AzugaUser struct {
	UserId uint
	ApiKey string
}

func GetAzugaUsers() []AzugaUser {
	azugaUsers := make([]AzugaUser, 0)
	db.DB.Raw("SELECT UserId, to_base64(ApiKey) as ApiKey FROM AzugaConfiguration WHERE IsActive = true").Scan(&azugaUsers)
	return azugaUsers
}
