package db

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

var (
	DB *gorm.DB
)

const (
	host     = "localhost"
	port     = 3306
	user     = "wrench"
	password = "Wr3nch!"
	dbname   = "wrench"
)

func InitDB() {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", user, password, host, port, dbname)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true, // use singular table name, table for `User` would be `user` with this option enabled
		},
		Logger:          logger.Default.LogMode(logger.Info),
		CreateBatchSize: 100,
	})
	if err != nil {
		panic(err)
	}
	DB = db
}
