package util

import (
	"log"
	"time"

	"wrench.com/azuga/config"
	"wrench.com/azuga/db"
)

func LockApp(app string, seconds int64) bool {
	if config.GetBoolean("com.wrench.cron.disabled", false) {
		log.Print("Crons disabled. Not running")
		return false
	}

	now := time.Now()
	var lock string
	result := db.DB.Raw("SELECT AppKey FROM AppResource WHERE AppKey = ?", app).Scan(&lock)
	if result.Error != nil {
		panic(result)
	}

	if result.RowsAffected > 0 {
		result = db.DB.Exec("UPDATE AppResource SET LockDate = ? WHERE AppKey = ? AND TIMESTAMPDIFF(SECOND, LockDate, ?) > ?", now, app, now, seconds)
		if result.Error != nil {
			panic(result)
		}
		return result.RowsAffected > 0
	} else {
		result = db.DB.Exec("INSERT INTO AppResource (AppKey, LockDate, CreateDate) VALUES (?, ?, ?)", app, now, now)
		if result.Error != nil {
			panic(result)
		}
		return result.RowsAffected > 0
	}
}
