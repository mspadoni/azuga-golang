package api

// AzugaVehicleResponse - full response body
type AzugaVehicleResponse struct {
	VehicleResponseVO VehicleResponseVO `json:"vehicleResponseVO"`
}
