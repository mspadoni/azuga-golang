package api

import (
	"wrench.com/azuga/db/models"
)

// VehicleResponseVO - inner object
type VehicleResponseVO struct {
	Code        int                   `json:"code"`
	Message     string                `json:"message"`
	MessageCode int                   `json:"messageCode"`
	Reason      string                `json:"reason"`
	GeneratedAt string                `json:"generatedAt"`
	CurrentPage int                   `json:"currentPage"`
	TotalPages  int                   `json:"totalPages"`
	Vehicles    []models.AzugaVehicle `json:"vehicles"`
}
