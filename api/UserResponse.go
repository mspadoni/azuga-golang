package api

import (
	"wrench.com/azuga/db/models"
)

// UserResponse - helper to group a user with a client response
type UserResponse struct {
	User     models.AzugaUser
	Response AzugaVehicleResponse
}
